
<div align="center">
<br/>
<br/>
<img src="readme/logo.png" width="90px" style="margin-top:30px;"/>
  <h1 align="center">
    TomatoSCRM
  </h1>
  <h4 align="center">
    企 业 宣 传 推 广 的 管 理 方 案
  </h4> 

  [预 览](http://#)   |   [官 网](http://www.pearadmin.com/)   |   [社区](http://forum.pearadmin.com/) |   [文档](http://www.pearadmin.com/doc)

</div>

<p align="center">
    <a href="#">
        <img src="https://img.shields.io/badge/Pear Admin-3.8.8+-green.svg" alt="Pear Admin Layui Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/Layui-2.6.4+-green.svg" alt="Layui Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/codeigniter-4.1.3-green.svg" alt="codeigniter 4">
    </a>
<a href="#">
        <img src="https://img.shields.io/badge/PHP-7.3.0+-red.svg" alt="PHP 7.3+">
    </a>
    
</p>
<br>
<div align="center">
  <img  width="92%" style="border-radius:10px;margin-top:20px;margin-bottom:20px;box-shadow: 2px 0 6px gray;" src="https://gitee.com/pear-admin/Pear-Admin-Layui/raw/master/admin/images/show.png" />
</div>
<br>

#### 项目介绍

<p style="padding:10px;"  width="90%">

本系统是采用Pear Admin Layui开箱即用的前后端解决方案 + 其它的Layui组件，

结合Codeigniter4开发而成。风格上延续LayuiAdmin风格，基于异步Ajax的菜单构建，为使用者提供简单、大方、美观的数据管理分析工具。

本项目开源免费，可直接拿去使用。只为让更多的宣传推广人员省时省力，项目不定时更新，建议 Star watch 一份。

</p>

#### 项目开发计划

开发计划说明：https://www.hanzhuoyou.cn/fengye/32.html

note：由于本项目是我一个人编写，时间不固定，碰到技术难题了还要在学习，所以进度较慢，请谅解！

#### 开源地址

Gitee 开源地址 : https://gitee.com/minsishuju/TomatoSCRM


#### 开源共建


1. 欢迎提交 [pull request](https://gitee.com/minsishuju/TomatoSCRM/pulls)，注意对应提交对应 `master` 分支

2. 欢迎提交 [issue](https://gitee.com/minsishuju/TomatoSCRM/issues)，请写清楚遇到问题的原因、复显步骤。

#### 特别感谢

特别感谢 @Jmysy ，本项目采用PearAdmin 模板；

感谢 layui、 Codeigniter等一系列开源项目。

#### 项目演示


